#!/bin/bash

## Number of threads
NUM_THREADS=8

## FASTA file for genome sequence
DB_FASTA="GRCm39.primary_assembly.genome.fa"

## Index name
DB_DIR="db.STAR/GRCm39_primary"

STAR --runThreadN $NUM_THREADS --runMode genomeGenerate \
  --genomeDir $DB_DIR \
  --genomeFastaFiles $DB_FASTA

# If you want to index a genome with annotation, 
# append the following line, together with a GTF filename.
# 
#--sjdbGTFfile $DB_GTF --sjdbOverhang 100
