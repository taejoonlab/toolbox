#!/usr/bin/env python3
import sys
import gzip

# polyN : 10 bp
# BC3 : 8 bp
# L2 linker: 30 bp (v2) or 12 bp (v3)
# BC2 : 8 bp
# L1 linker: 22 bp (v2) or 12 bp (v3)
# BC1 : 8 bp

len_polyN = 10
len_BC3 = 8
len_L2 = 30
len_BC2 = 8
len_L1 = 22
len_BC1 = 8

filename_fq = sys.argv[1]
f_fq = open(filename_fq, 'r')
if filename_fq.endswith('.gz'):
    f_fq = gzip.open(filename_fq, 'rt')

f_out = open('%s.bc_list' % filename_fq, 'w')
for line in f_fq:
    if line.startswith('@'):
        tmp_h = line.strip()
        tmp_nseq = next(f_fq).strip()
        tmp_h2 = next(f_fq)
        tmp_qseq = next(f_fq)

        tmp_start = len_polyN
        tmp_BC3 = tmp_nseq[tmp_start:tmp_start+len_BC3]
        tmp_start += len_BC3 + len_L2
        tmp_BC2 = tmp_nseq[tmp_start:tmp_start+len_BC2]
        tmp_start += len_BC2 + len_L1
        tmp_BC1 = tmp_nseq[tmp_start:tmp_start+len_BC1]

        f_out.write("%s\t%s\t%s\t%s\n" % (tmp_h.split()[0], tmp_BC1, tmp_BC2, tmp_BC3))
f_fq.close()
f_out.close()
# @A00479:488:H5VV2DSXC:1:1101:1217:1063 2:N:0:ACTGATAG+GTCCAACC
# TATTG AGTCG ATCATTCC GTGGCCGATGTTTCGCATCGGCGTACGACT GTACGCAA ATCCACGTGCTTGAGACTGTGG CATTCCTA TTTTTTTTTTTTTAT
# @A00479:488:H5VV2DSXC:1:1101:1235:1063 2:N:0:ACTGATAG+GTCCAACC
# TCCGA CTGCT ACACGACC GTGGCCGATGTTTCGCATCGGCGTACGACT ACGCTCGA ATCCACGTGCTTGAGACTGTGG CCAATTCT TTTTTTTTTTTTTTT
# @A00479:488:H5VV2DSXC:1:1101:1452:1063 2:N:0:ACTGATAG+GTCCAACC
# CTTCG TTGGA GCGAGTAA GTGGCCGATGTTTCGCATCGGCGTACGACT GACAGTGC ATCCACGTGCTTGAGACTGTGG TGCTGCTC TCGGGCTGGGCCTCG
